const { createMachine, interpret } = require("xstate");
const pool = require("./database");

const getMachine = async (workflowId, initialState, context = {}) => {
  const machineConf = await pool.query(
    `SELECT * FROM workflows WHERE workflow_id = ${workflowId} LIMIT 1`
  );

  const { workflow_config } = machineConf.rows[0];

  if (initialState) workflow_config.initial = initialState.trim();

  const machine = createMachine(workflow_config, {
    guards: {
      isLowerThanMinimumScore: ({ score }) => score < 500,
      isGreaterThanMinimumScore: ({ score }) => score >= 500,
    },
  });

  return machine.withContext(context);
};

const getNextState = (machine, abnormal = false) => {
  const workflowEngine = interpret(machine);
  workflowEngine.start();
  workflowEngine.send(workflowEngine.state.nextEvents[abnormal ? 1 : 0]);
  workflowEngine.off();
  return workflowEngine.state.value;
};

module.exports = { getMachine, getNextState };
