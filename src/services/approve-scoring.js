const { default: axios } = require("axios");
const { getMachine, getNextState } = require("../create-machine");
const pool = require("../database");

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
const approveScoring = async (req, res) => {
  try {
    const { customer_id, approved } = req.body;

    const query = await pool.query(
      `SELECT * FROM customers WHERE customer_id = '${customer_id}'`
    );
    const { workflow_id, current_state } = query.rows[0];

    const machine = await getMachine(workflow_id, current_state);
    const nextState = getNextState(machine, !approved);

    await axios.post("/approveCustomerScore", {
      id: customer_id,
      scoreApproved: approved,
    });

    await pool.query(
      `UPDATE customers SET current_state = '${nextState}' WHERE customer_id = '${customer_id}'`
    );

    res.status(200).json({
      message: approved ? "scoring was approved" : "scoring was not approved",
      next_state: nextState,
    });
  } catch (error) {
    res.status(200).json({ message: error.message });
  }
};

module.exports = approveScoring;
