const { default: axios } = require("axios");
const { getMachine, getNextState } = require("../create-machine");
const pool = require("../database");

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
const assingScore = async (req, res) => {
  try {
    const { customer_id, score } = req.body;
    const query = await pool.query(
      `SELECT * FROM customers WHERE customer_id = '${customer_id}'`
    );

    const { workflow_id, current_state } = query.rows[0];

    const machine = await getMachine(workflow_id, current_state, { score });
    const nextState = getNextState(machine);

    await axios.post("/assignCustomerScore", {
      id: customer_id,
      score: score,
    });

    await pool.query(
      `UPDATE customers SET current_state = '${nextState}' WHERE customer_id = '${customer_id}'`
    );

    res
      .status(200)
      .json({ message: "scoring assigned", score, next_state: nextState });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = assingScore;
