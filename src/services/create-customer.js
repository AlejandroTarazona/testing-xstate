const { default: axios } = require("axios");
const { getMachine, getNextState } = require("../create-machine");

const pool = require("../database");

/**
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
const createCustomer = async (req, res) => {
  try {
    const { workflow_id, company_name, email, annual_revenue } = req.body;
    const machine = await getMachine(workflow_id);
    const nextState = getNextState(machine);

    const { data } = await axios.post("/createCustomer", {
      companyName: company_name,
      email: email,
      annualRevenue: annual_revenue,
    });
    await pool.query(
      `INSERT INTO customers (customer_id, current_state, workflow_id) VALUES ('${data.customerId}', '${nextState}', ${workflow_id})`
    );
    res.status(200).json({
      message: "customer created",
      customer_id: data.customerId,
      next_state: nextState,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = createCustomer;
