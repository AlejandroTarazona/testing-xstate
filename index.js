require("dotenv").config();
const approveScoring = require("./src/services/approve-scoring");
const assingScore = require("./src/services/assing-scoring");
const createCustomer = require("./src/services/create-customer");
const express = require("express");
const morgan = require("morgan");
const routes = require("./src/routes");
const { default: axios } = require("axios");

const app = express();

app.use(morgan("dev"));
app.use(express.json());

axios.interceptors.request.use((config) => {
  config.baseURL = process.env.API_HOST;
  return config;
});

app.listen(process.env.PORT, () => {
  console.log("Listen on port:", process.env.PORT);
});

app.post(routes.CREATE_CUSTOMER, createCustomer);
app.post(routes.CALCULATE_SCORING, assingScore);
app.post(routes.APPROVE, approveScoring);
